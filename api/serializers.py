from rest_framework import serializers
from tasks import models

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Task
        fields = (
            'id',
            'user',
            'title',
            'status',
            'description',
            'due_date',
            'calculate_time'
        )