from tasks.models import Task
from .serializers import TaskSerializer
from rest_framework.response import Response
from rest_framework.decorators import action

from rest_framework import viewsets

class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    @action(methods=['get'],detail=False)
    def userTask(self,request):
        user_task = self.get_queryset().filter(user=request.auth.user.id)
        serializer = self.get_serializer_class()(user_task,many=True)
        return Response(serializer.data)