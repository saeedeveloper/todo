# Todo Django Project

# Goal
Simple todo web application with RESTful API. This app help users to manage tasks on a project.

# Requirements
To install Django requirement please see the requirements.txt
Here is list of installed app used:
- Crispy Form
- Django REST Framework

# Object define
- User can add, update and delete own task.
- User can see all task add by anybody and detail of them.
- User have their own profile can see information and upload photo profile.

Task include:
- title
- status
- description
- due date and time
- created time

# Models
### UserProfile
- user = on to on relation with build in Django User
- avatar = user profile picture
- firstname
- lastname
- email
### Tasks
- title
- status = (DRAFT,STARTED, PENDING, FINISHED)
- description
- due_date
- created_at

# REST Framework details
## Login API
POST /api-token-auth
body include: username & password

**test account**
username:john
password:1234@qaz
response = token

## Saving Task API
POST /api/v1/tasks with token in header Authorization: Token --recevice token from Login API

## Update Task API
### for update some feild
PATCH /api/v1/tasks/postid with token in header Authorization: Token --recevice token from Login API
### for update whole record
PUT /api/v1/tasks/postid with token in header Authorization: Token --recevice token from Login API

## Deleting Task API
### for delete record
DELETE /api/v1/tasks/postid/ with token in header Authorization: Token recevice token from Login API

## Retreving Task API
### for all tasks
GET /api/v1/tasks with token in header Authorization: Token --recevice token from Login API
### for user's task
GET /api/v1/tasks/userTask with token in header Authorization: Token --recevice token from Login API

## App Guide
Task status details
- Draft: Not sure about task but you want to be in list
- Started: Task that are currently in progress
- Pending: Task paused for any reason such as some one should do somethign for that 
- Finished: Completed task.