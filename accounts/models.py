from django.db import models
from django.contrib.auth.models import User
# Create your models here.

"""
user profile model work with user built in
"""
class UserProfile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
    firstname = models.CharField(max_length=255,blank=True)
    lastname = models.CharField(max_length=255,blank=True)
    email = models.EmailField(max_length=255,null=True)
    avatar = models.ImageField(upload_to='media',default='default_avatar.png')

    def __str__(self):
        return self.user.username.capitalize()