from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .models import UserProfile
from .forms import UserProfileForm

"""
Class for login user
"""
class LoginUser(View):
    template_name = 'accounts/login.html'

    def get(self,request):
        return render(request,self.template_name)

    
    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('tasks:task_list')
        else:
            messages.error(request, 'Username or Password is incorrect!!!')
            return render(request,self.template_name)

"""
class for logout user
"""
class LogoutUser(View):

    def post(self,request,*args,**kwargs):
        logout(request)
        return redirect('accounts:login')

"""
class for profile page
"""
class UserProfilePage(View):
    template_name = 'accounts/profile.html'

    def get(self,request,id):
        login_user = UserProfile.objects.get(user_id=id)
        form = UserProfileForm(instance=login_user)
        context = {
            'login_user':login_user,
            'form':form
        }
        return render(request,self.template_name,context)

    def post(self,request,id):
        login_user = UserProfile.objects.get(user_id=id)
        form = UserProfileForm(request.POST,request.FILES,instance=login_user)
        if form.is_valid():
            form.save()

        return redirect(request.path)