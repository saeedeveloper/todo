from django.urls import path
from .views import LoginUser,LogoutUser,UserProfilePage
from django.contrib.auth.decorators import login_required


app_name='accounts'

urlpatterns = [
	path('login/', LoginUser.as_view(), name="login"),
	path('logout/', login_required(LogoutUser.as_view()), name="logout"),
	path('profile/<str:id>', login_required(UserProfilePage.as_view()),name="profile")
]