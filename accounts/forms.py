from django import forms
from .models import UserProfile
from django.contrib.auth.models import User

"""
This form about profile model without user
"""
class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = '__all__'
        exclude = ['user']