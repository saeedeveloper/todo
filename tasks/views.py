from django.shortcuts import render,redirect
from django.views import View
from .models import Task
from accounts.models import UserProfile
from .forms import CreateTaskForm

# Create your views here.

"""
This is index page and show list of user's tasks and all other tasks.
We sure that user should login to access to this page. user can view, update and delete own task
with button in each row
"""
class TaskList(View):
    template_name = 'tasks/index.html'
    def get(self,request):
        login_user = UserProfile.objects.get(user=request.user) # get login user

        allTask = Task.objects.exclude(user_id=request.user.id) # read all task exclude login user task
        myTask = Task.objects.filter(user_id=request.user.id)   # read user's tasks
        taskStarted = myTask.filter(status='Started').count()   # count user's task with started status
        taskPending = myTask.filter(status='Pending').count()   # count user's task with pending status
        taskFinished = myTask.filter(status='Finished').count() # count user's task with finished status

        # make context for render HTML template
        context = {
            'login_user':login_user,
            'task_started':taskStarted,
            'task_pending':taskPending,
            'task_finished':taskFinished,
            'all_task':allTask,
            'my_task':myTask
        }
        return render(request,self.template_name,context)

"""
This is user's task page for showing only user's task. user can view, update and delete tasks
"""
class MyTask(View):
    template_name = 'tasks/my_task.html'

    def get(self,request):
        login_user = UserProfile.objects.get(user=request.user) # get login user
        myTask = Task.objects.filter(user_id=request.user.id)   # read user's tasks
        
        # make context for render HTML template
        context = {
            'login_user':login_user,
            'my_task':myTask,
        }
        return render(request,self.template_name, context)

"""
This is all task page for showing all other task. user just can view other tasks
"""
class AllTask(View):
    template_name = 'tasks/all_task.html'
    def get(self,request):
        login_user = UserProfile.objects.get(user=request.user) # get login user
        allTask = Task.objects.exclude(user_id=request.user.id) # read user's tasks

        # make context for render HTML template
        context = {
            'login_user':login_user,
            'all_task':allTask
        }

        return render(request,self.template_name,context)

"""
This view is about create task. use form model for creating task
"""
class CreateTask(View):
    template_name = 'tasks/create_task.html'
    def get(self,request,id):
        login_user = UserProfile.objects.get(user=request.user) # get login user
        form = CreateTaskForm(instance=login_user)

        # make context for render HTML template
        context = {
            'form':form
        }
        return render(request,self.template_name,context)

    def post(self,request,id):
        login_user = UserProfile.objects.get(user=request.user) # get login user
        form = CreateTaskForm(request.POST)
        # check form is valid or not
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = login_user
            instance.save()
            return redirect('/')

"""
This view is about update task.
"""
class UpdateTask(View):
    template_name = 'tasks/create_task.html'

    def get(self,request,id):
        task = Task.objects.get(id=id)
        form = CreateTaskForm(instance=task)
        context = {
            'form':form
        }
        return render(request,self.template_name,context)

    def post(self,request,id):
        task = Task.objects.get(id=id)
        form = CreateTaskForm(request.POST,instance=task)

        if(form.is_valid()):
            form.save()
            return redirect('/')

"""
This view is about delete task.
"""
class DeleteTask(View):
    template_name = 'tasks/delete_task.html'

    def get(self,request,id):
        task = Task.objects.get(id=id)
        context={
            'task':task
        }
        return render(request,self.template_name,context)

    def post(self,request,id):
        task = Task.objects.get(id=id)
        task.delete()
        return redirect('/')

"""
This view is about view task.
"""
class ViewTask(View):
    template_name = 'tasks/view_task.html'

    def get(self,request,id):
        login_user = UserProfile.objects.get(user=request.user)
        task = Task.objects.get(id=id)
        context = {
            'login_user':login_user,
            'task':task
        }
        return render(request,self.template_name,context)