from django.contrib import admin
from django.urls import path, include
from .views import TaskList,MyTask,AllTask,CreateTask,ViewTask,UpdateTask,DeleteTask
from django.contrib.auth.decorators import login_required

app_name='tasks'

urlpatterns = [
    path('',login_required(TaskList.as_view()),name='task_list'),
    path('my_task/',login_required(MyTask.as_view()), name='my_task'),
    path('all_task/',login_required(AllTask.as_view()),name='all_task'),
    path('create_task/<str:id>',login_required(CreateTask.as_view()),name="create_task"),
    path('view_task/<str:id>',login_required(ViewTask.as_view()),name="view_task"),
    path('update_task/<str:id>',login_required(UpdateTask.as_view()),name="update_task"),
    path('delete_task/<str:id>',login_required(DeleteTask.as_view()),name="delete_task"),
]