from django import forms
from .models import Task

"""
Model form for creating task
all fileds in form exclude user
"""
class CreateTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        exclude = ['user']