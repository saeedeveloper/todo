from django.db import models
from accounts.models import UserProfile
from django.utils import timezone

# Create your models here.

"""
Task model class will keep user's task with title, status, description, and due date.
"""
class Task(models.Model):
    # status feilds choices
    STATUS = (
        ('Draft', 'Draft'),
        ('Started', 'Started'),
        ('Pending', 'Pending'),
        ('Finished', 'Finished')
    )
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, default=None)
    title = models.CharField(max_length=255)
    status = models.CharField(max_length=10, choices=STATUS)
    description = models.TextField()
    due_date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    # calculate time remaining to due date.
    def calculate_time(self):
        now = timezone.now()
        time = self.due_date - now
        days = time.days
        seconds = time.seconds
        hours = seconds//3600

        return str(days) + " days, " + str(hours) + " hours"

    # return part text of description
    def descriptionSnipp(self):
        return self.description[:15] + ' ...'
